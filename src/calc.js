//TODO: arithmetic operations
/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number} 
 */
const add = (a, b) => a+b;

const subtract = (minuened, subtrahend) => {
    return minuened - subtrahend;
};

const multiply = (multiplier, multiplicant) =>{
    return multiplier*multiplicant;
};
/**
 * 
 * @param {number} divident 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (divident, divisor) => {
    if (divisor == 0) throw new Error("0 division is not allowed");
    const fraction = divident/divisor;
    return fraction

};

export default { add, subtract, divide, multiply }