#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-test
code ~/projects/stam/L03/unit-test/

git init
npm init -y

npm install --save express
npm install --save mocha chai

echo "node_modules" > .gitignore

git remote add origin https://gitlab.com/maleena.nordstrom/unit-test
git push -u origin master

touch README.d
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

git add . --dry-run
